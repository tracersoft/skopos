# Skopos [![BuildStatus](https://travis-ci.org/tracersoft/skopos.svg?branch=add-travis-ci)](https://travis-ci.org/tracersoft/skopos)

## Getting Started

First you need to install the dependency Memcache:

Ubuntu:
`sudo apt-get install memcached`

For other SO, please check [http://www.memcached.org/downloads]: http://www.memcached.org/downloads 

Then clone this repo.:

`git clone git@github.com:tracersoft/skopos.git`

Copy the file `.env.sample` to `.env` and set yours local configurations.

Now you just need to run this setup script to set up your machine
with the necessary dependencies to run and test this app:

   ` % ./bin/setup`

It assumes you have a machine equipped with Ruby, Postgres, etc. If not, set up
your machine with [this script].

[this script]: https://github.com/thoughtbot/laptop

After setting up, you can run the application using [foreman]:

    % foreman start

If you don't have `foreman`, see [Foreman's install instructions][foreman]. It
is [purposefully excluded from the project's `Gemfile`][exclude].

[foreman]: https://github.com/ddollar/foreman
[exclude]: https://github.com/ddollar/foreman/pull/437#issuecomment-41110407

## Guidelines

Use the following guides for getting things done, programming well, and
programming in style.

* [Protocol](http://github.com/thoughtbot/guides/blob/master/protocol)
* [Best Practices](http://github.com/thoughtbot/guides/blob/master/best-practices)
* [Style](http://github.com/thoughtbot/guides/blob/master/style)

## Deploying

If you have previously run the `./bin/setup` script,
you can deploy to staging and production with:

    $ ./bin/deploy staging
    $ ./bin/deploy production
