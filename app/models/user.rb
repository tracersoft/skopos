class User < ActiveRecord::Base
  include Clearance::User
  enum user_type: [ :admin, :client ]

  def set_token
    self.api_token = ENV['API_TOKEN'] unless admin? 
  end
end
