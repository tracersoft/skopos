require "#{Rails.root}/lib/my_token_authentication"

Her::API.setup url: "https://www.pivotaltracker.com/services/v5" do |c|
  # Request
  c.use MyTokenAuthentication
  c.use FaradayMiddleware::Caching, Memcached::Rails.new('127.0.0.1:11211')

  # Response
  c.use Her::Middleware::DefaultParseJSON

  # Adapter
  c.use Faraday::Adapter::NetHttp
end
