class DropTableProjects < ActiveRecord::Migration
  def change
    drop_table :projects do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
