class MyTokenAuthentication < Faraday::Middleware
  def call(env)
    env[:request_headers]["X-TrackerToken"] = RequestStore.store[:my_api_token]
    @app.call(env)
  end
end
