FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@example.com"
  end

  factory :user do
    api_token "12345"
    email
    password "password"

    trait :client do
      user_type :client 
    end
    
    trait :admin do
      user_type :admin 
    end

  end
end
