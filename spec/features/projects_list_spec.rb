require 'spec_helper'


feature 'Projects list' do
  before do
    stub_request(:post, "https://www.pivotaltracker.com/services/v5/projects/projects").
      with(:body => {"name"=>"MyString"},
      :headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type'=>'application/x-www-form-urlencoded', 'User-Agent'=>'Faraday v0.9.1'}).
           to_return(:status => 200, :body => "", :headers => {})

    stub_api_for(Project) do |stub|
      stub.get("/projects") { |env| [200, {}, [{ id: 1, name: "Skopos" }, { id: 2, name: "Plataforma" }].to_json] }
    end
  end

  scenario 'user is authenticated' do
    user = create(:user)
    visit projects_path(as: user)
    Project.all.each do |project|
      expect(page).to have_text(project.name)
    end
  end
end
