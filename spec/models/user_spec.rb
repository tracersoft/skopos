require 'rails_helper'

describe User do
  describe "#set_token" do
    before do
     stub_const('ENV', {'API_TOKEN' => '67890'}) 
    end
    context "user is admin" do
      it "returns api_token from DB" do
        user_adm = create(:user, :admin)        
        user_adm.set_token
        expect(user_adm.api_token).to eql("12345")
      end    
    end

    context "user is client" do
      it "returns api_token from ENV variable" do
        user_client = create(:user, :client)        
        user_client.set_token
        expect(user_client.api_token).to eql(ENV['API_TOKEN'])       
      end
    end
  end
end
